package com.sda.matrice;

import java.io.*;

public class VerificColoaneMatrice {

    // iau dimensiunile matricei din fisier, dupa care iau elementele matricei
    public Matrice citescMatrice(String patch) {
        Matrice matrice = new Matrice();
        File inputFile = new File(patch);

        try {
            FileReader fileReader = new FileReader(inputFile);
            BufferedReader reader = new BufferedReader(fileReader);

            String linie = reader.readLine();
            String[] dimensiuniMatrice = linie.split(" ");
            int[][] matrix = new int[Integer.valueOf(dimensiuniMatrice[0])][Integer.valueOf(dimensiuniMatrice[1])];

            matrice.setNrLinii(Integer.valueOf(dimensiuniMatrice[0]));
            matrice.setNrCol(Integer.valueOf(dimensiuniMatrice[1]));

            linie = reader.readLine();
            int indexLinie = 0;

            while (linie != null) {
                String[] linieMatrice = linie.split(" ");
                for (int i = 0; i < linieMatrice.length; i++) {
                    matrix[i][indexLinie] = Integer.valueOf(linieMatrice[i]); // ca sa pot parsa matricea pe coloane in metoda de verificare a sumei celei mai mari, creez matricea din fisier invers (practic inversez liniile cu coloanele)
                }
                indexLinie++;
                linie = reader.readLine();
            }
            matrice.setMatrice(matrix);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return matrice;
    }

    // creez un array cu elementele matricei cu suma cea mai mare
    public int[] coloanaMatrice(Matrice matrice) {
        int maxim = 0;
        int[] temp = null;

        for (int i = 0; i < matrice.getNrLinii(); i++) {
            int[] arr = matrice.getMatrice()[i];
            int sum = 0;
            for (int j = 0; j < arr.length; j++) {
                sum = sum + arr[i];
            }
            if (maxim < sum) {
                maxim = sum;
                temp = arr;
            }
        }
        return temp;
    }

    // scriu array-ul in fisier
    public void scriuMatriceaInFisier(int[] array) {
        File file = new File("C:\\Users\\vasile\\Desktop\\coloana.txt");

        try {
            FileWriter writer = new FileWriter(file);
            BufferedWriter bufferedWriter = new BufferedWriter(writer);
            bufferedWriter.write("Afisez coloana din matrice, cu suma numerelor cea mai mare: ");
            bufferedWriter.newLine();

            for (int i = 0; i < array.length; i++) {
                bufferedWriter.write(String.valueOf(array[i]));
                bufferedWriter.newLine();
            }
            bufferedWriter.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

}
