package com.sda.matrice;

public class Matrice {
    private int nrLinii;
    private int nrCol;
    private int[][] matrice;

    public Matrice() {
    }

    public int getNrLinii() {
        return nrLinii;
    }

    public void setNrLinii(int nrLinii) {
        this.nrLinii = nrLinii;
    }

    public int getNrCol() {
        return nrCol;
    }

    public void setNrCol(int nrCol) {
        this.nrCol = nrCol;
    }

    public int[][] getMatrice() {
        return matrice;
    }

    public void setMatrice(int[][] matrice) {
        this.matrice = matrice;
    }
}
